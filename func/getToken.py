import os
from func import commandLineArgs
from settings.config import settings


def get_token():
    if settings['token'] is not None:
        token = settings['token']
    elif os.environ.get('KEY') is not None:
        token = os.environ.get('KEY')
    elif commandLineArgs.args.token is not None:
        token = commandLineArgs.args.token
    else:
        token = None
    return token
