import discord
import requests
from settings.config import settings, ImgUrls
import random as true_random


def random_pic_or_gif(ctx, act):
    if act.lower() in ImgUrls:
        response = requests.get(ImgUrls[act.lower()])
        title = act
    else:
        response = requests.get(true_random.choice(list(ImgUrls.values())))
        title = 'Full RANDOM'
    json_data = response.json()
    embed = discord.Embed(color=settings['EmbedColor'], title=title)
    embed.set_image(url=json_data['link'])
    return ctx.send(embed=embed)
