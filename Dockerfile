FROM ubuntu:22.04
COPY . /bot
WORKDIR /bot
RUN apt-get update && apt-get install git ffmpeg python3 python3-pip -y
RUN pip3 install youtube_dl
RUN pip3 install -r requirements.txt
RUN ffmpeg -version
ENTRYPOINT ["python3", "/bot/main.py"]
