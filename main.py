#!/usr/bin/python3
import discord
import os
from func import getToken
from discord.ext import commands
from settings.config import settings

bot = commands.Bot(command_prefix=settings['prefix'])


@bot.event
async def on_ready():
    print(f'{bot.user} has logged in!')
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name='Autism'))


for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        bot.load_extension(f"cogs.{filename[:-3]}")


token = getToken.get_token()

if token is None:
    print('set token')
else:
    bot.run(token)
