facts_dict = {
    'dog': 'https://some-random-api.ml/animal/dog',
    'cat': 'https://some-random-api.ml/animal/cat',
    'panda': 'https://some-random-api.ml/animal/panda',
    'redpanda': 'https://some-random-api.ml/animal/red_panda',
    'bird': 'https://some-random-api.ml/animal/birb',
    'fox': 'https://some-random-api.ml/animal/fox',
    'koala': 'https://some-random-api.ml/animal/koala',
    'racoon': 'https://some-random-api.ml/animal/raccoon',
    'kangaroo': 'https://some-random-api.ml/animal/kangaroo'
}

facts = ('dog', 'cat', 'panda', 'redpanda', 'bird', 'fox', 'koala', 'racoon', 'kangaroo')

url = 'https://some-random-api.ml/animal/'
