settings = {
    'token': None,
    'bot': 'jarvis test',
    'id': 661652528869539850,
    'prefix': '=',
    'EmbedColor': 0x013220
}

ImgUrls = {
    'dog': 'https://some-random-api.ml/img/dog',
    'cat': 'https://some-random-api.ml/img/cat',
    'panda': 'https://some-random-api.ml/img/panda',
    'redpanda': 'https://some-random-api.ml/img/red_panda',
    'bird': 'https://some-random-api.ml/img/birb',
    'fox': 'https://some-random-api.ml/img/fox',
    'koala': 'https://some-random-api.ml/img/koala',
    'racoon': 'https://some-random-api.ml/img/racoon',
    'kangaroo': 'https://some-random-api.ml/img/kangaroo',
    'whale': 'https://some-random-api.ml/img/Whale',
    'hug': 'https://some-random-api.ml/animu/hug',
    'pat': 'https://some-random-api.ml/animu/pat',
    'wink': 'https://some-random-api.ml/animu/wink',
    'pikachu': 'https://some-random-api.ml/img/pikachu',
    'face-palm': 'https://some-random-api.ml/animu/face-palm'
}
RandomCommandsList = [
    'img', 'gif'
]
ydl_opts = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'
}
ffmpeg_opt = {
    'options': '-vn'
}
ytKey = 'AIzaSyBoF0FhesaMGoDxbIUG9DeaMU0ACHu8AUA'
