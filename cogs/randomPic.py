from discord.ext import commands
from func.randomPicOrGif import random_pic_or_gif
from settings.config import RandomCommandsList


class RandomPic(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def random(self, ctx, *args):
        """random pic or gif"""
        if args:
            elements = list(args)
            element = elements.pop(0)
            await random_pic_or_gif(ctx, element, *elements)
        else:
            await ctx.send('you can use next commands')
            for i in RandomCommandsList:
                await ctx.send(i)


def setup(bot):
    bot.add_cog(RandomPic(bot))
