import discord
import pafy
import asyncio
from yarl import URL
from settings import config
from discord.ext import commands
from queue import Queue
from discord.utils import get


playlist = {}
pafy.set_api_key(config.ytKey)


class Music(commands.Cog):
    def __init__(self, bot):
        """Music player"""
        self.bot = bot

    @commands.group()
    async def music(self, ctx):
        pass

    @music.command(pass_context=True, aliases=['p'])
    async def play(self, ctx, *argv):
        join = await self.join(ctx)
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        await self._queue(ctx, *argv)
        if join:
            if not voice.is_playing() and not playlist[ctx.guild].empty():
                video = playlist[ctx.guild].get()
                source = await discord.FFmpegOpusAudio.from_probe(video.streams[0].url)
                voice.play(source, after=lambda e=None: asyncio.run_coroutine_threadsafe(self.play_next(ctx), voice.loop))
                await ctx.send(f'Now playing {video.title}')
                await ctx.channel.edit(topic=f'Now Playing {video.title}')
            else:
                await ctx.channel.edit(topic=None)
                await ctx.send("nothing to play")

    @music.command(pass_context=True, aliases=['queue'])
    async def _queue(self, ctx, *argv):
        for i in argv:
            url = URL(i)
            if url.scheme == "https":
                if ctx.guild in playlist:
                    playlist[ctx.guild].put(pafy.new(i))
                else:
                    playlist[ctx.guild] = Queue()
                    playlist[ctx.guild].put(pafy.new(i))

    @music.command(pass_context=True)
    async def pause(self, ctx):
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        if voice.is_playing():
            voice.pause()

    @music.command(pass_context=True)
    async def resume(self, ctx):
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        if voice.is_paused():
            voice.resume()

    @music.command(pass_context=True)
    async def skip(self, ctx):
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        if voice.is_playing():
            voice.stop()

    @music.command(pass_context=True, aliases=['j'])
    async def join(self, ctx):
        if ctx.author.voice:
            channel = ctx.author.voice.channel
            voice = get(self.bot.voice_clients, guild=ctx.guild)
            if voice and voice.is_connected():
                await voice.move_to(channel)
            else:
                await channel.connect()
            return True
        else:
            await ctx.send("you not in voice channel")
            return False

    @music.command(pass_context=True, aliases=['l', 'dc', 'disconnect'])
    async def leave(self, ctx):
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        if voice and voice.is_connected:
            await ctx.voice_client.disconnect()
        else:
            await ctx.send("i'm not in voice channel")
        await ctx.channel.edit(topic=None)

    async def play_next(self, ctx):
        await self.play(ctx)


def setup(bot):
    bot.add_cog(Music(bot))
