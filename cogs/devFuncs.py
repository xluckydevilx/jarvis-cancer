import discord
from discord.ext import commands


class DevFuncs(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def load(self, ctx, extension):
        self.bot.load_extension(f"cogs.{extension}")
        await ctx.send(f"cog {extension} loaded...")

    @commands.command()
    async def unload(self, ctx, extension):
        self.bot.unload_extension(f"cogs.{extension}")
        await ctx.send(f"cog {extension} unloaded...")

    @commands.command()
    async def reload(self, ctx, extension):
        self.bot.reload_extension(f"cogs.{extension}")
        await ctx.send(f"cog {extension} reloaded...")


def setup(bot):
    bot.add_cog(DevFuncs(bot))
