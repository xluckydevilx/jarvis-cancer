from discord.ext import commands
from discord.utils import get
from settings.factConf import facts, url
from settings.config import settings
from googletrans import Translator
import requests
import random as true_random
import googletrans
import discord


translator = Translator()


class Fact(commands.Cog):
    def __init__(self, bot):
        """random facts about animal"""
        self.bot = bot

    @commands.command()
    async def fact(self, ctx, *args):
        """random facts about animal"""
        element = 'moo'
        if args:
            elements = list(args)
            element = elements.pop(0)
        if element not in facts:
            element = true_random.choice(facts)
        if element == 'bird':
            element = 'birb'
        response = requests.get(url+element)
        json_data = response.json()
        embed = discord.Embed(color=settings['EmbedColor'], title=element)
        embed.set_image(url=json_data['image'])
        fact = translator.translate(text=json_data['fact'], dest='ru')
        embed.set_footer(text=fact.text)
        return await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fact(bot))
